alias la="ls -A"
alias rm="rm -vi"
alias mv="mv -v"
alias irbmacs="irb --nocolorize --nomultiline"
alias feh="feh -d"
alias sl="echo \'steam locomotive\'"
alias term="sh ~/.xmonad/term"
alias dterm="sh ~/.xmonad/term 1"
export DISPLAY=:0
export TERM=xterm-256color
LANG="en_US.UTF-8"
LANGUAGE="en_US.UTF-8"
# LC_ALL="en_US.UTF-8"
# PS1='\w: ' # prompt is just `directory: `
PS1='; ' # prompt is just `directory: `
echo -e -n "\x1b[\x30 q" # changes cursor to blinking block

# This way we don't get that annoying GUI prompt when submitting git credentials
unset SSH_ASKPASS

export PATH=$PATH:"/home/meriadoc/scripts/"
export PATH=$PATH:"/home/meriadoc/.local/share/gem/ruby/3.0.0/bin"
export PATH=$PATH:"/home/meriadoc/Documents/processing/processing-3.5.4"

PATH="/home/meriadoc/perl5/bin${PATH:+:${PATH}}";export PATH;
PERL5LIB="/home/meriadoc/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="/home/meriadoc/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"/home/meriadoc/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=/home/meriadoc/perl5"; export PERL_MM_OPT;
