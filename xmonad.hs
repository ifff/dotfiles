{-# LANGUAGE FlexibleContexts #-}

import qualified Data.Map as M

import XMonad

import XMonad.Actions.FloatKeys

import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks

import XMonad.Layout.Accordion
import XMonad.Layout.BinarySpacePartition
import XMonad.Layout.Gaps
import XMonad.Layout.Grid
import XMonad.Layout.LayoutModifier
import XMonad.Layout.Spacing
import XMonad.Layout.Tabbed
import XMonad.Layout.TabBarDecoration

import XMonad.Prompt
import XMonad.Prompt.Shell

import qualified XMonad.StackSet as W

import XMonad.Util.Run (spawnPipe)
import XMonad.Util.EZConfig (additionalKeys)
import XMonad.Util.Loggers
import XMonad.Util.SpawnOnce (spawnOnce)
import XMonad.Util.Themes

import System.IO

data Direction = North | South | East | West
  deriving (Eq, Show)

main :: IO ()
main = do
  let conf = defaultConfig
        { manageHook = manageDocks <+> manageHook defaultConfig
        , layoutHook = avoidStruts $ myLayout
        , keys = \c -> myKeys c `M.union` keys defaultConfig c
        , modMask = myModMask
        , focusedBorderColor = myFocusedBorderColor
        , normalBorderColor = myNormalBorderColor
        , borderWidth = 1
        , focusFollowsMouse = False
        , terminal = "sh /home/meriadoc/.xmonad/term"
        , startupHook = myStartupHook }

  (xmonad . docks) conf

-- preliminary stuff to execute (Order 66 not included)
myStartupHook = spawnOnce "sh /home/meriadoc/.xmonad/autostart.sh"

-- if you want resizeable BSP borders:
-- https://hackage.haskell.org/package/xmonad-contrib-0.13/docs/XMonad-Layout-BinarySpacePartition.html
myLayout = emptyBSP ||| gappyBSP ||| Full
  where tiled = gaps [(U,20), (R,20), (D,20), (L,20)] $ smartSpacing 20 $ Tall nmaster delta ratio
        gappyBSP = gaps [(U,20), (R,20), (D,20), (L,20)] $ smartSpacing 20 $ emptyBSP
        nmaster = 1
        delta = 3/100
        ratio = 1/2

myTheme :: Theme
myTheme = def
  { activeColor = "#945ca8"
  , inactiveColor = "#000000"
  -- , fontName = "xft:Code New Roman:pixelsize=12"
  }

myShellPromptCfg :: XPConfig
myShellPromptCfg = def
  { bgColor  = "#000000"
  , fgColor  = "#ffffff"
  , promptBorderWidth = 0
  , position = Top
  , height = 18
  -- , font = "xft:Code New Roman:pixelsize=14"
  }
-- myShellPromptCfg = greenXPConfig
-- myShellPromptCfg = def
--   { bgColor = "#000000"
--   , promptBorderWidth = 0
--   , position = Bottom }

-- custom commands
myKeys (XConfig { modMask = modm, terminal = term }) = M.fromList $
  [ -- launch firefox
  ((modm, xK_b), spawn "firefox; exit")
    -- scr
  , ((modm, xK_l), spawn "import ~/Pictures/scr.png")
  -- launch shell prompt
  , ((modm, xK_p), spawn "dmenu_run -nf \"#a0adee\" -nb black -sb \"#a0adee\" -sf black")
  -- toggle floating window
  , ((modm, xK_t), toggleFloat)
  -- the following are some cmus-remote stuff
  , ((noModMask, xK_F4), spawn "cmus-remote -C 'toggle repeat_current'") -- toggle repeat
  , ((noModMask, xK_F5), spawn "cmus-remote -S") -- toggle shuffle
  , ((noModMask, xK_F10), spawn "cmus-remote -r") -- prev song
  , ((noModMask, xK_F10), spawn "cmus-remote -u") -- pause/unpause
  , ((noModMask, xK_F11), spawn "cmus-remote -n") -- next song
  -- mpv-remote stuff
  , ((modm, xK_i), spawn "~/Documents/ruby/mpv-remote/mpv-remote prev") -- mpv prev song
  , ((modm, xK_o), spawn "~/Documents/ruby/mpv-remote/mpv-remote next") -- mpv next song
  , ((modm, xK_y), spawn "~/Documents/ruby/mpv-remote/mpv-remote pause") -- mpv pause
  , ((modm, xK_u), spawn "~/Documents/ruby/mpv-remote/mpv-remote upause") -- mpv unpause
  -- rebind opening terminal to mod4+enter
  , ((modm, xK_Return), spawn term)
  -- launch dark mode terminal
  , ((modm .|. shiftMask, xK_Return), spawn "sh /home/meriadoc/.xmonad/term 1")
  -- incr volume by 1%
  , ((noModMask, xK_F8), spawn "amixer -q sset Master 1%+ unmute")
  -- decr volume by 1%
  , ((noModMask, xK_F7), spawn "amixer -q sset Master 1%- unmute")
  -- toggle mute/unmute
  , ((noModMask, xK_F6), spawn "amixer -q sset Master toggle")
  -- switch to a random wallpaper
  , ((modm, xK_r), spawn "sh /home/meriadoc/.xmonad/wp -r")
  -- switch to the next wallpaper
  , ((modm, xK_w), spawn "sh /home/meriadoc/.xmonad/wp -n")
  -- switch to the previous wallpaper
  , ((modm .|. shiftMask, xK_w), spawn "sh /home/meriadoc/.xmonad/wp -p")
  -- set the current wallpaper to tiled mode
  , ((modm .|. shiftMask, xK_t), spawn "sh /home/meriadoc/.xmonad/wp -c -m --bg-tile")
  -- set the current wallpaper to fill mode
  , ((modm .|. shiftMask, xK_f), spawn "sh /home/meriadoc/.xmonad/wp -c -m --bg-fill")
  -- set the current wallpaper to centered mode
  , ((modm, xK_c), spawn "sh /home/meriadoc/.xmonad/wp -c -m --bg-center")
  -- for moving floating windows (using vim keys)
  , ((modm .|. controlMask, xK_h), moveWindow' West)
  , ((modm .|. controlMask, xK_j), moveWindow' South)
  , ((modm .|. controlMask, xK_k), moveWindow' North)
  , ((modm .|. controlMask, xK_l), moveWindow' East)
  -- for resizing floating windows
  , ((modm, xK_a), shrinkWindow East)
  , ((modm, xK_s), shrinkWindow South)
  , ((modm .|. shiftMask, xK_a), growWindow East)
  , ((modm .|. shiftMask, xK_s), growWindow South)]

-- use Super key instead of Meta for Mod
myModMask = mod4Mask

toggleFloat = withFocused toggle
  where toggle winid = do
          floats <- gets (W.floating . windowset)
          if winid `M.member` floats
            then withFocused (windows . W.sink)
            else do
            keysMoveWindowTo (50, 50) (0, 0) winid
            keysResizeWindow (125, 200) (0, 0) winid

moveWindow' East = withFocused (keysMoveWindow (5, 0))
moveWindow' West = withFocused (keysMoveWindow (-5, 0))
moveWindow' North = withFocused (keysMoveWindow (0, -5))
moveWindow' South = withFocused (keysMoveWindow (0, 5))
  
shrinkWindow East = withFocused (keysResizeWindow (-10, 0) (0, 0))
shrinkWindow South = withFocused (keysResizeWindow (0, -10) (0, 0))
growWindow East = withFocused (keysResizeWindow (10, 0) (0, 0))
growWindow South = withFocused (keysResizeWindow (0, 10) (0, 0))

myNormalBorderColor = "#000"
myFocusedBorderColor = myFGColor
myFGColor = "#000" --"#268bd2"
myBGColor = "black"
